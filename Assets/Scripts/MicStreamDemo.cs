﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using HoloToolkit.Unity;
using System.IO;
using Renci.SshNet;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

[RequireComponent(typeof(AudioSource))]
public class MicStreamDemo : MonoBehaviour
{
    #region ClassMembers
    public MicStream.StreamCategory StreamType = MicStream.StreamCategory.LOW_QUALITY_VOICE;

    System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();

    public Text comp;

    /// <summary>
    /// Can boost volume here as desired. 1 is default.
    /// <remarks>Can be updated at runtime.</remarks> 
    /// </summary>
    public float InputGain = 1;

    bool startUploading = false;

    SftpClient sftp = null;

    string result = "";

    bool VocalCommands = true;

    /// <summary>
    /// if keepAllData==false, you'll always get the newest data no matter how long the program becomes unresponsive
    /// for any reason. It will lose some data if the program does not respond.
    /// <remarks>Can only be set on initialization.</remarks>
    /// </summary>
    public bool KeepAllData;

    /// <summary>
    /// If true, Starts the mic stream automatically when this component is enabled.
    /// </summary>
    public bool AutomaticallyStartStream = true;

    /// <summary>
    /// Plays back the microphone audio source though default audio device.
    /// </summary>
    public bool PlaybackMicrophoneAudioSource = false;

    /// <summary>
    /// The name of the file to which to save audio (for commands that save to a file).
    /// </summary>
    public string SaveFileName = "MicrophoneTest";

    public string deSalvat = "";

    public int contor = 0;

    public int numaraCadre = 0;

    private bool isRunning;

    private Vector3 offset = new Vector3(0, 2, 6);            //Private variable to store the offset distance between the player and camera

    public bool IsRunning
    {
        get { return isRunning; }
        private set
        {
            isRunning = value;
            CheckForErrorOnCall(isRunning ? MicStream.MicPause() : MicStream.MicResume());
        }
    }

    #endregion
    // Start is called before the first frame update
    void Start()
    {
        connectToCloud();
        downloadMP3("Salutare, utilizator!");
        try
        {
            CheckForErrorOnCall(MicStream.MicStartStream(KeepAllData, false));
            //watch.Start();
            if (GetComponent<Text>() != null)
                Debug.Log("Textul din componenta: " + GetComponent<Text>().text);
           // StartRecording();
        }
        catch (Exception ex)
        {
            Debug.Log("[Start exceptie] " + ex.Source + "\n" + ex.Message + "\n" + ex.Data);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!VocalCommands)
            return;

        //GetComponent<Text>().transform.position = GazeManager.Instance.HitPosition +  new Vector3(0, 1, 7);
        //    if (watch.IsRunning)
        //        GetComponent<Text>().text = "Reactionez la comunicarea verbala ";
        //    else
        //        GetComponent<Text>().text = "Procesez limbajul verbal";
        if (watch.ElapsedMilliseconds >= 2500)
        {
            startUploading = true;

            if (result != "")
            {
                procesareComanda(result);
            }
        }
    }

    private void setIsRunningUploading()
    {
        while (true)
        {
            while (!startUploading)
                continue;
            watch.Stop();
            watch.Reset();
            StopRecording();

            
            watch.Start();
            StartRecording();
            //m.WaitOne();
            startUploading = false;
            // m.ReleaseMutex();
        }
    }

    private void OnEnable()
    {
        IsRunning = true;
    }

    private void Awake()
    {
        //if (!VocalCommands)
        //    return;
        CheckForErrorOnCall(MicStream.MicInitializeCustomRate((int)StreamType, AudioSettings.outputSampleRate));
        CheckForErrorOnCall(MicStream.MicSetGain(InputGain));
        var t = Task.Run(new Action(setIsRunningUploading));
        

        isRunning = true;

    }


    void StartRecording()
    {
        deSalvat = SaveFileName + "_" + contor.ToString() + ".wav";
        CheckForErrorOnCall(MicStream.MicStartRecording(deSalvat, false));
        contor++;
    }

    void StopRecording()
    {
        //Debug.Log("StopRecording was called!");
        string outputPath = MicStream.MicStopRecording();
        if (outputPath == "YouArentRecordingRightNow")
        {
            StartRecording();
            return;
        }
        upload(outputPath);
    }

    #region audioAlgorithms
    /// <summary>
    /// Converts two bytes to one float in the range -1 to 1.
    /// </summary>
    /// <param name="firstByte">The first byte.</param>
    /// <param name="secondByte"> The second byte.</param>
    /// <returns>The converted float.</returns>
    private static float BytesToFloat(byte firstByte, byte secondByte)
    {
        // Convert two bytes to one short (little endian)
        short s = (short)((secondByte << 8) | firstByte);

        // Convert to range from -1 to (just below) 1
        return s / 32768.0F;
    }

    /// <summary>
    /// Dynamically creates an <see cref="AudioClip"/> that represents raw Unity audio data.
    /// </summary>
    /// <param name="name"> The name of the dynamically generated clip.</param>
    /// <param name="audioData">Raw Unity audio data.</param>
    /// <param name="sampleCount">The number of samples in the audio data.</param>
    /// <param name="frequency">The frequency of the audio data.</param>
    /// <returns>The <see cref="AudioClip"/>.</returns>
    private static AudioClip ToClip(string name, float[] audioData, int sampleCount, int frequency)
    {
        var clip = AudioClip.Create(name, sampleCount, 1, frequency, false);
        clip.SetData(audioData, 0);
        return clip;
    }

    /// <summary>
    /// Converts an array of bytes to an integer.
    /// </summary>
    /// <param name="bytes"> The byte array.</param>
    /// <param name="offset"> An offset to read from.</param>
    /// <returns>The converted int.</returns>
    private static int BytesToInt(byte[] bytes, int offset = 0)
    {
        int value = 0;
        for (int i = 0; i < 4; i++)
        {
            value |= ((int)bytes[offset + i]) << (i * 8);
        }
        return value;
    }

    /// <summary>
    /// Converts raw WAV data into Unity formatted audio data.
    /// </summary>
    /// <param name="wavAudio">The raw WAV data.</param>
    /// <param name="sampleCount">The number of samples in the audio data.</param>
    /// <param name="frequency">The frequency of the audio data.</param>
    /// <returns>The Unity formatted audio data. </returns>
    private static float[] ToUnityAudio(byte[] wavAudio, out int sampleCount, out int frequency)
    {
        // Determine if mono or stereo
        int channelCount = wavAudio[22];  // Speech audio data is always mono but read actual header value for processing

        // Get the frequency
        frequency = BytesToInt(wavAudio, 24);

        // Get past all the other sub chunks to get to the data subchunk:
        int pos = 12; // First subchunk ID from 12 to 16

        // Keep iterating until we find the data chunk (i.e. 64 61 74 61 ...... (i.e. 100 97 116 97 in decimal))
        while (!(wavAudio[pos] == 100 && wavAudio[pos + 1] == 97 && wavAudio[pos + 2] == 116 && wavAudio[pos + 3] == 97))
        {
            pos += 4;
            int chunkSize = wavAudio[pos] + wavAudio[pos + 1] * 256 + wavAudio[pos + 2] * 65536 + wavAudio[pos + 3] * 16777216;
            pos += 4 + chunkSize;
        }
        pos += 8;

        // Pos is now positioned to start of actual sound data.
        sampleCount = (wavAudio.Length - pos) / 2;  // 2 bytes per sample (16 bit sound mono)
        if (channelCount == 2) { sampleCount /= 2; }  // 4 bytes per sample (16 bit stereo)

        // Allocate memory (supporting left channel only)
        var unityData = new float[sampleCount];

        // Write to double array/s:
        int i = 0;
        while (pos < wavAudio.Length)
        {
            unityData[i] = BytesToFloat(wavAudio[pos], wavAudio[pos + 1]);
            pos += 2;
            if (channelCount == 2)
            {
                pos += 2;
            }
            i++;
        }

        return unityData;
    }
    #endregion

    string post(string name, string port)
    {
        using (var httpClient = new HttpClient())
        {
            string uri = "http://" + "34.66.27.105:" + port + "/";

            using (var request = new HttpRequestMessage(new HttpMethod("POST"), uri))
            {
                request.Content = new StringContent(name);
                request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("text/plain");

                var task = httpClient.SendAsync(request);
                // Debug.Log("Am trimis requestul avand continutul: " + name  + " la adresa " + uri);
                task.Wait();
                var response = task.Result;
                return response.Content.ReadAsStringAsync().Result;
            }
        }
        //return "";
    }

    void procesareComanda(string comanda)
    {
        Debug.Log("Am primit comanda: " + comanda);
        //GameObject[] objects = SceneManager.GetActiveScene().GetRootGameObjects();
        //if (LCS(comanda.ToCharArray(), "pornește animația".ToCharArray()) > 77.0 || LCS(comanda.ToCharArray(), "pornești animația".ToCharArray()) > 77.0)
        //{

        //    for (int i = 0; i < objects.Length; i++)
        //    {
        //        Animation anim = objects[i].GetComponent<Animation>();
        //        if (anim != null)
        //            anim.Play();
        //    }

        //}
        //else if (LCS(comanda.ToCharArray(), "oprește animatia".ToCharArray()) > 77.0 || LCS(comanda.ToCharArray(), "oprești animația".ToCharArray()) > 77.0)
        //{

        //    for (int i = 0; i < objects.Length; i++)
        //    {
        //        Animation anim = objects[i].GetComponent<Animation>();
        //        if (anim != null)
        //            anim.Stop();
        //    }
        //}
        //isRecordingStarted = false;
    }

    private float LCS(char[] str1, char[] str2)
    {
        int str1Length = str1.Length;
        int str2Length = str2.Length;
        int[,] l = new int[str1Length, str2Length];
        int maxLength = str1Length;
        if (str1Length < str2Length)
            maxLength = str2Length;
        int lcs = -1;
        string substr = string.Empty;
        int end = -1;

        for (int i = 0; i < str1.Length; i++)
        {
            for (int j = 0; j < str2.Length; j++)
            {
                if (str1[i] == str2[j])
                {
                    if (i == 0 || j == 0)
                    {
                        l[i, j] = 1;
                    }
                    else
                        l[i, j] = l[i - 1, j - 1] + 1;
                    if (l[i, j] > lcs)
                    {
                        lcs = l[i, j];
                        end = i;
                    }

                }
                else
                    l[i, j] = 0;
            }
        }

        for (int i = end - lcs + 1; i <= end; i++)
        {
            substr += str1[i];
        }
        float res = (float)lcs / maxLength * 100;
        return res;
    }

    private void OnApplicationPause(bool pause)
    {
        IsRunning = !pause;
    }

    private void OnDisable()
    {
        IsRunning = false;
    }

    private void OnDestroy()
    {
        CheckForErrorOnCall(MicStream.MicDestroy());

    }

#if !UNITY_EDITOR
        private void OnApplicationFocus(bool focused)
        {
            IsRunning = focused;
        }
#endif


    #region SFTP
    void connectToCloud()
    {
        string host = @"34.66.27.105";
        string username = "cpamparau";
        string password = "cvscvs";

        sftp = new SftpClient(host, username, password);
        try
        {
            sftp.Connect();
            Debug.Log("We are connected on SFTP!");
        }
        catch (Exception e)
        {
            Debug.Log("We couldn't connect to Google Cloud Virtual Machine for Uploading/Downloading files." + e.Message + "\n" + e.Source);
        }
    }

    private void upload(string fileName)
    {
        if (sftp == null || !sftp.IsConnected)
        {
            Debug.Log("[Upload] We don't have a SFTP connection!");
            return;
        }

        string remoteDirectory = "/home/cpamparau/" + deSalvat;

        using (Stream fileStream = File.OpenRead(fileName))
        {
            try
            {
                sftp.UploadFile(fileStream, remoteDirectory);
                result = post(deSalvat, "8000");
            }
            catch (Exception ex)
            {
                Debug.Log("Exceptie: " + ex.Message);
            }
        }
    }

    public void downloadMP3(string inputString)
    {
        if (sftp == null || !sftp.IsConnected)
        {
            Debug.Log("[Download] We don't have a SFTP connection!");
            return;
        }
        if (inputString == "")
        {
            return;
        }
        string re = post(inputString, "8001");
        string path = "/home/cpamparau/" + re;
        string DownloadedPath = Path.Combine(Application.persistentDataPath, re);
        using (FileStream writer = File.Create(DownloadedPath))
        {
            try
            {
                sftp.DownloadFile(path, writer);
                sftp.DeleteFile(path);
                Debug.Log("Am primit de la Google Cloud TTS   " + re);
            }
            // return res;
            catch (Exception ex)
            {
                Debug.Log("Exceptie: " + ex.Message);
            }
        }
        int sampleCount = 0;
        int frequency = 0;
        Byte[] buffer = File.ReadAllBytes(DownloadedPath);
        AudioSource audioSource = GetComponent<AudioSource>();
        var unityData = ToUnityAudio(buffer, out sampleCount, out frequency);

        // Convert to an audio clip
        var clip = ToClip("Speech", unityData, sampleCount, frequency);

        // Set the source on the audio clip
        audioSource.clip = clip;
        // audioSource.volume = 2f;

        // Play audio
        audioSource.Play();
    }
    #endregion


    private static void CheckForErrorOnCall(int returnCode)
    {
        MicStream.CheckForErrorOnCall(returnCode);
    }

    public void Enable()
    {
        IsRunning = true;
    }

    public void Disable()
    {
        IsRunning = false;
    }
}

