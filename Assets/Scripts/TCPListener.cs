﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

#if !UNITY_EDITOR
    using Windows.Networking;
    using Windows.Networking.Sockets;
    using Windows.Storage.Streams;
#endif

//Able to act as a reciever 
public class TCPListener : MonoBehaviour
{
    public String _input = "";

#if !UNITY_EDITOR
        StreamSocket socket;
        StreamSocketListener listener = null;
        String port = "";
        String message;
#endif

    // Use this for initialization
    void Start()
    {
        //Debug.Log("Am pornit????");
#if !UNITY_EDITOR
        //listener = new StreamSocketListener();
        //port = "1337";
        //listener.ConnectionReceived += Listener_ConnectionReceived;
        //listener.Control.KeepAlive = false;

        //Listener_Start();
#endif
    }

#if !UNITY_EDITOR
    private async void Listener_Start()
    {
        Debug.Log("Listener started");
        try
        {
            await listener.BindServiceNameAsync(port);
        }
        catch (Exception e)
        {
            Debug.Log("Error: " + e.Message);
        }

        Debug.Log("Listening");
    }

    private async void Listener_ConnectionReceived(StreamSocketListener sender, StreamSocketListenerConnectionReceivedEventArgs args)
    {
        Debug.Log("Connection received");

        try
        {
            //while (true) {

                using (var dr = new DataReader(args.Socket.InputStream))
                {
                    dr.InputStreamOptions = InputStreamOptions.Partial;
                    //size of bytes to load each time.
                    uint sizeToReadEachTime = 2048;
                    CancellationTokenSource cts = new CancellationTokenSource();
                    //set timeout here.
                    cts.CancelAfter(5000);
                    ulong max = 0;
                    _input = "";

                    while (true)
                    {
                        try
                        {
                            //add a timeout for the asynchronous load operation.
                            var count = await dr.LoadAsync(sizeToReadEachTime).AsTask(cts.Token);
                            max += count;
                             if (count == 0)
                                throw new System.Threading.Tasks.TaskCanceledException();
                            
                            //byte[] buffer = new byte[26];
                            //dr.ReadBytes(buffer);
                            _input += dr.ReadString(count);
                            //_input += System.Text.Encoding.UTF8.GetString(buffer);
                         }
                        catch (System.Threading.Tasks.TaskCanceledException)
                        {
                            //load operation will get timeout only when there is no data available.
                            cts.Dispose();
                            break;
                        }
                    }
                    Debug.Log("max=" + max);
                    Debug.Log("BodyFrame [ " + _input.Length+ "]: " + _input);
                    dr.DetachStream();
 
                }

                using (var dw = new DataWriter(args.Socket.OutputStream))
                {
                    uint f = dw.WriteString("done");
                    Debug.Log("done length=" + f);
                    await dw.StoreAsync(); 
                //args.Socket.Contro
                    dw.DetachStream();
                }



            //}
        }
        catch (Exception e)
        {
            Debug.Log("disconnected!!!!!!!! " + e.Message + "\n" + e.StackTrace + "\n" + e.Source);
        }

    }

#endif

    void Update()
    {
        //Debug.Log("Input: " + _input);
        //this.GetComponent<TextMesh>().text = _input;
    }
}