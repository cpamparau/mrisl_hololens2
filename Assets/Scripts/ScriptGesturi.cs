﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.Input;

public class ScriptGesturi : MonoBehaviour, IMixedRealityTouchHandler
{

    #region internalMethods
    private ClasaSingleton instantaSingleton = null;
    private int fromStates(ClasaSingleton.States state)
    {
        switch (state)
        {
            case ClasaSingleton.States.INTENSE_ANGER:
                return 0;
            case ClasaSingleton.States.MODERATE_ANGER:
                return 1;
            case ClasaSingleton.States.LOW_ANGER:
                return 2;
            case ClasaSingleton.States.NEUTRAL:
                return 3;
            case ClasaSingleton.States.LOW_JOY:
                return 4;
            case ClasaSingleton.States.MODERATE_JOY:
                return 5;
            case ClasaSingleton.States.INTENSE_JOY:
                return 6;
            default:
                return 100; // UNDEFINED STATE
        }
    }

    private ClasaSingleton.States toState(int state)
    {
        switch (state)
        {
            case 0:
                return ClasaSingleton.States.INTENSE_ANGER;
            case 1:
                return ClasaSingleton.States.MODERATE_ANGER;
            case 2:
                return ClasaSingleton.States.LOW_ANGER;
            case 3:
                return ClasaSingleton.States.NEUTRAL;
            case 4:
                return ClasaSingleton.States.LOW_JOY;
            case 5:
                return ClasaSingleton.States.MODERATE_JOY;
            case 6:
                return ClasaSingleton.States.INTENSE_JOY;
            default:
                return ClasaSingleton.States.UNDEFINED_STATE;
        }
    }

    // matrix for equation:
    // line: Avatar's expression in the previous trial
    // column: Participant response's in the previous trial
    // access the matrix: matrix[2,3]
    private int[,] matrix = new int[7, 7]
    {
            {0, 6, 5, 4, 3, 2, 1 },
            {2, 1, 0, 6, 5, 4, 3 },
            {4, 3, 2, 1, 0, 6, 5 },
            {6, 5, 4, 3, 2, 1, 0 },
            {1, 0, 6, 5, 4, 3, 2 },
            {3, 2, 1, 0, 6, 5, 4 },
            {5, 4, 3, 2, 1, 0, 6 }
    };

    private ClasaSingleton.States choice(string nameOfGameObject)
    {
        if (nameOfGameObject.Contains("0"))
            return ClasaSingleton.States.INTENSE_ANGER;
        else if (nameOfGameObject.Contains("1"))
            return ClasaSingleton.States.MODERATE_ANGER;
        else if (nameOfGameObject.Contains("2"))
            return ClasaSingleton.States.LOW_ANGER;
        else if (nameOfGameObject.Contains("3"))
            return ClasaSingleton.States.NEUTRAL;
        else if (nameOfGameObject.Contains("4"))
            return ClasaSingleton.States.LOW_JOY;
        else if (nameOfGameObject.Contains("5"))
            return ClasaSingleton.States.MODERATE_JOY;
        else if (nameOfGameObject.Contains("6"))
            return ClasaSingleton.States.INTENSE_JOY;
        else
            return ClasaSingleton.States.UNDEFINED_STATE;
    }

    #endregion

    public void buton(GameObject gameObject)
    {
        Debug.Log("Trial " + instantaSingleton.countTrial.ToString());
        int linie, coloana;
        ClasaSingleton.States result;
        string result_anim = "";
        string initial = "";
        if (instantaSingleton.isFirstTrial)
        {
            //Debug.Log("Inainte de trial-ul " + SpawnItems.Instance.countTrial.ToString() + ", avatar_expr_prev=" + SpawnItems.Instance.avatar_expression_in_the_previous_trial.ToString() + 
            //   ", previous_response=" + SpawnItems.Instance.participants_response_in_the_previous_trial.ToString());
            instantaSingleton.participants_response_in_the_previous_trial = choice(gameObject.name);
            Debug.Log("Ai ales this.gameObject.name= " + gameObject.name);
            linie = fromStates(instantaSingleton.avatar_expression_in_the_previous_trial);
            coloana = fromStates(instantaSingleton.participants_response_in_the_previous_trial);
            result = toState(matrix[linie, coloana]);

            initial = (fromStates(instantaSingleton.avatar_expression_in_the_previous_trial) + 1).ToString() + "_" + (fromStates(result) + 1).ToString();
            result_anim = (fromStates(result) + 1).ToString() + "_" + (fromStates(result) + 1).ToString();
            instantaSingleton.animator.Play(initial); // run the transition
            Debug.Log(initial + "  " + result_anim);
            instantaSingleton.animator.Play(result_anim);
            instantaSingleton.avatar_expression_in_the_previous_trial = result;
            Debug.Log("Dupa trial-ul " + instantaSingleton.countTrial.ToString() + ", avatar_expr_prev(noul result)=" + instantaSingleton.avatar_expression_in_the_previous_trial.ToString() +
                ", previous_response(alegerea facuta la acest trial)=" + instantaSingleton.participants_response_in_the_previous_trial.ToString());
            instantaSingleton.isFirstTrial = false;
            instantaSingleton.deactivateOrActivateObects(false);
            instantaSingleton.watch.Start();
            instantaSingleton.countTrial++;
            return;
        }
        Debug.Log("Inainte de trial-ul " + instantaSingleton.countTrial.ToString() + ", avatar_expr_prev=" + instantaSingleton.avatar_expression_in_the_previous_trial.ToString() +
        ", previous_response=" + instantaSingleton.participants_response_in_the_previous_trial.ToString());
        linie = fromStates(instantaSingleton.avatar_expression_in_the_previous_trial);
        coloana = fromStates(choice(gameObject.name));
        Debug.Log("Ai ales this.gameObject.name= " + gameObject.name);
        if (toState(coloana) == instantaSingleton.participants_response_in_the_previous_trial)
        {
            // nu permitem acelasi raspuns, repetam trial-ul (deci nu incrementam trial-ul)
            // avertizare utilizator ca a dat acelasi raspuns si ca trebuie sa repete alegerea pentru trial-ul curent
            // momentan facem doar return, aceeasi semnificatie
            Debug.Log("Ai dat acelasi raspuns consecutiv, nu este ok! Cronometrul este pornit? " + instantaSingleton.watch.IsRunning.ToString());
            if (instantaSingleton.watch.IsRunning)
            {
                instantaSingleton.watch.Stop();
                instantaSingleton.watch.Reset();
            }
            return;
        }
        result = toState(matrix[linie, coloana]);
        // actualizeaza avatarul dupa result (animatia)
        initial = (fromStates(instantaSingleton.avatar_expression_in_the_previous_trial) + 1).ToString() + "_" + (fromStates(result) + 1).ToString();
        result_anim = (fromStates(result) + 1).ToString() + "_" + (fromStates(result) + 1).ToString();
        instantaSingleton.animator.Play(initial); // run the transition
        Debug.Log(initial + "  " + result_anim);
        instantaSingleton.animator.Play(result_anim);
        instantaSingleton.participants_response_in_the_previous_trial = toState(coloana);
        instantaSingleton.avatar_expression_in_the_previous_trial = result;
        Debug.Log("Dupa trial-ul " + instantaSingleton.countTrial.ToString() + ", avatar_expr_prev(noul result)=" + instantaSingleton.avatar_expression_in_the_previous_trial.ToString() +
                ", previous_response(alegerea facuta la acest trial)=" + instantaSingleton.participants_response_in_the_previous_trial.ToString());
        instantaSingleton.deactivateOrActivateObects(false); // deactivate objects
        instantaSingleton.watch.Start();
        instantaSingleton.countTrial++;
    }

    // Start is called before the first frame update
    void Start()
    {
        instantaSingleton = ClasaSingleton.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void IMixedRealityTouchHandler.OnTouchStarted(HandTrackingInputEventData eventData)
    {
        Debug.Log("[OnTouchStarted] Trial: " + instantaSingleton.countTrial.ToString());
    }

    void IMixedRealityTouchHandler.OnTouchCompleted(HandTrackingInputEventData eventData)
    {
        Debug.Log("[OnTouchCompleted] Trial: " + instantaSingleton.countTrial.ToString());
        buton(this.gameObject);
        //entData.selectedObject
    }

    void IMixedRealityTouchHandler.OnTouchUpdated(HandTrackingInputEventData eventData)
    {
        //Debug.Log("[OnTouchUpdated] Trial: " + instantaSingleton.countTrial.ToString());
    }
}
