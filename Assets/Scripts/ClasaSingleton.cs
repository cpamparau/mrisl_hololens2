﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity; // manually imported from HoloToolkitCompatibilityPack

public class ClasaSingleton : Singleton<ClasaSingleton>
{
    //public Transform cameraPosition;

    public SpriteRenderer next;

    public GameObject[] faces;

    public MeshRenderer Chair;

    public GameObject Avatar;

    public Animator animator = null;

    public System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();

    public bool isFirstTrial = true;

    public int countTrial = 0;

    public enum States
    {
        INTENSE_ANGER = 0,
        MODERATE_ANGER = 1,
        LOW_ANGER = 2,
        NEUTRAL = 3,
        LOW_JOY = 4,
        MODERATE_JOY = 5,
        INTENSE_JOY = 6,
        UNDEFINED_STATE = 100
    };

    public void deactivateOrActivateObects(bool deactivate)
    {
        for (int i = 0; i < faces.Length; i++)
            faces[i].SetActive(deactivate);
    }

    public States avatar_expression_in_the_previous_trial = States.INTENSE_ANGER; // used for accessing the line of the below matrix

    public States participants_response_in_the_previous_trial = States.INTENSE_ANGER; // used for accessing the column of the below matrix


    // Start is called before the first frame update
    void Start()
    {
      
        //Camera.main.transform.position = new Vector3(-0.1f, 1.68f, -5.01f);
        //camera.transform = new Vector3((float)-0.57, (float)4.68, (float)-6.41);
        for (int i = 0; i < faces.Length; i++)
            faces[i].SetActive(true);
        Chair.enabled = true;
        //Avatar = Instantiate(Avatar);
        animator = Avatar.GetComponent<Animator>();
		animator.SetFloat("runMultiplier", 0.02f);
		animator.speed = 0.02f;
        //if (animator == null)
        //    Debug.Log("[ClasaSingleton-Start] Animator=ul este NULL!");
        //var x = animator.runtimeAnimatorController;
        //var y = x.animationClips;
        //for (int i = 0; i < y.Length; i++)
        //    Debug.Log(y[i].name);
    }

    // Update is called once per frame
    void Update()
    {
        //Camera.main.transform.position = new Vector3(-0.57f, 4.68f, -6.41f);
        if (watch.ElapsedMilliseconds >= 2000)
        {
            watch.Stop();
            watch.Reset();
            Debug.Log("Am oprit cronometrul pentru Trial-ul " + countTrial.ToString());
            deactivateOrActivateObects(true);
        }
    }
}
